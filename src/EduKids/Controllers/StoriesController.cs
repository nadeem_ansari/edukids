﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace EduKids.Controllers
{
    public class StoriesController : Controller
    {
        // GET: /<controller>/
        public IActionResult booksForYoungChildren()
        {

            return View();
        }

        public IActionResult theGingerbreadMan()
        {
            return View();
        }      

        public IActionResult theGingerbreadManPage2 ()
        {
            return View();
        }

        public IActionResult theGingerbreadManPageFinal()
        {
            return View();
        }
    }
}
